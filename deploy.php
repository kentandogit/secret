<?php
namespace Deployer;

require 'recipe/laravel.php';

// Project name
set('application', 'my_project');

// Project repository
set('repository', 'git@gitlab.com:kentandogit/secret.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', false);

// Shared files/dirs between deploys
add('shared_files', ['resources/lang/en.json', 'resources/lang/cn.json']);
add('shared_dirs', []);

// Writable dirs by web server
add('writable_dirs', ['resources/lang']);
set('allow_anonymous_stats', false);

// Hosts
host('staging')
    ->hostname('178.128.84.50')
    ->user('kent')
    ->stage('staging')
    ->set('branch', 'staging')
    ->set('deploy_path', '/var/www/html/secret.com')
    ->addSshOption('StrictHostKeyChecking', 'no');

host('production')
    ->hostname('178.128.84.50')
    ->user('kent')
    ->stage('production')
    ->set('branch', 'master')
    ->set('deploy_path', '/var/www/html/secret.com')
    ->addSshOption('StrictHostKeyChecking', 'no');

// Tasks

task('build', function () {
    run('cd {{release_path}} && build');
});

task(
    'reload:php-fpm',
    function () {
//        run('sudo systemctl reload php7.2-fpm');
    }
);

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');
after('deploy', 'reload:php-fpm');

// Migrate database before symlink new release.

before('deploy:symlink', 'artisan:migrate');
after('artisan:migrate', 'artisan:db:seed');
