<?php

namespace App\Repositories;

use App\Models\Secret;
use Carbon\Carbon;

class SecretRepository
{
    /**
     * $data = ["key"=> "value"]
     *
     * @param $data
     */
    public function processSecret($data)
    {
        $newSecret = [];
        $timestamp = null;
        foreach ($data as $key => $value) {
            $timestamp = time();
            $newSecret[] = [
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'timestamp'  => $timestamp,
                    'key'        => $key,
                    'value'      => $value
                ];
            // we can process multiple key => value pair
            // but for now lets just process one
            break;
        }

        Secret::insert($newSecret);
        return $timestamp;
    }

    /**
     * Will return the latest value of the key
     * If timestamp is provided -
     * Will return the latest value of the key on the timestamp
     *
     * @param $key
     * @param null $timestamp
     * @return mixed
     */
    public function getSecret($key, $timestamp = null)
    {
        // if timestamp is empty or null this will be
        // the current datetime
        /**
         * when testing using the created_at datetime from db be sure to pass the timezone
         * to get the correct UTC datetime/timestamp equivalent because time() is timezone independent
         * but strtotime() is not e.g. strtotime('2020-05-24 13:04:05 GMT+8') when the key is created
         * at 1:04PM SGT time
         */
        if (!$timestamp) {
            $timestamp = time();
        }

        return Secret::where('key', $key)->where('timestamp', '<=', $timestamp)
            ->orderBy('created_at', 'desc')->first();
    }
}
