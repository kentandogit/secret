<?php

namespace App\Http\Controllers;

use App\Http\Requests\Store;
use App\Repositories\SecretRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SecretController extends Controller
{
    protected $secretRepository;

    public function __construct(SecretRepository $secretRepository)
    {
        // set rbac permissions here if we have
        // but currently we dont are not required

        $this->secretRepository = $secretRepository;
    }

    /**
     * We dont have use for index function for now so lets return a
     * message to prevent users encountering 404 response for accessing
     * api/object
     *
     * @param Request $request
     * @return JsonResource
     */
    public function index(Request $request)
    {
        return response()->json(["success" => false, "message" => "not found", "data" => []], 404);
    }

    /**
     * Accepts a key => value pair and save it to the DB
     * we can easily make this accept multiple key => value pair
     * but for now lets just process the first one and ignore the rest
     *
     * @param Store $request
     * @return JsonResource
     */
    public function store(Store $request)
    {
        try {
            DB::beginTransaction();
            $timestamp = $this->secretRepository->processSecret($request->all());
            DB::commit();
            return JsonResource::make(['timestamp' => $timestamp])
                ->additional(['success' => true, 'message' => 'key processed successfully']);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::info($e);
            return JsonResource::make([])
                    ->additional(['success' => false, 'message' => 'failed to process key']);
        }
    }

    /**
     * Will return the latest value of the key
     * If timestamp is provided in the query string -
     * Will return the latest value of the key on the timestamp
     *
     * @param Request $request
     * @return mixed
     */
    public function show(Request $request, $object)
    {
        try {
            $secret = $this->secretRepository->getSecret($object, $request->timestamp);

            // we dont return standard json response?
            if ($secret) {
                return $secret->value;
            }

            return JsonResource::make([])
                ->additional(['success' => false, 'message' => 'key not found']);
        } catch (\Exception $e) {
            Log::info($e);
            return JsonResource::make([])
                    ->additional(['success' => false, 'message' => 'something went wrong and we are fixing problem']);
        }
    }
}
