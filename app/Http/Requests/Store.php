<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Store extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            '*' => ['required']
        ];

        return $rules;
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $all = $this->request->all();
            if (empty($all)) {
                $validator->errors()->add('key', 'no key provided');
            }
        });
    }

    /**
     * The error message for required - can be change to a proper one
     *
     * @return array
     */
    public function messages()
    {
        return [
            '*.required' => 'must have a value',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
