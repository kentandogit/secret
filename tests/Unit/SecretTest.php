<?php

namespace Tests\Unit;

use App\Models\Secret;
use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class SecretTest extends TestCase
{
    use RefreshDatabase;

    public function testUrlNotFound()
    {
        $response = $this->get('/object');
        $response->assertStatus(404);
    }

    public function testSaveKey()
    {
        $faker = Factory::create();
        $faker->seed(rand(1, 100));
        $key   = $faker->name;
        $value = $faker->text;

        $this->postJson('/api/object', [$key => $value])
            ->assertStatus(200);

        $this->assertDatabaseHas('secrets', ['key' => $key, 'value' => $value]);

        $response = $this->getJson('/api/object/' . $key);
        $response->assertStatus(200);
        $response->assertSee($value);

        $response = $this->getJson('/api/object/' . $key . '?timestamp=' . time());
        $response->assertStatus(200);
        $response->assertSee($value);
    }

    public function testEmptyPayload()
    {
        $this->postJson('/api/object', [])
            ->assertStatus(422);
    }

    public function testWrongPayload()
    {
        $faker = Factory::create();
        $faker->seed(rand(1, 100));
        $key   = $faker->name;
        $value = $faker->text;

        $response = $this->postJson('/api/object', [$key, $value]);
        $response->assertStatus(422);
    }

    public function testKeyNotFound()
    {
        $response = $this->getJson('/api/object/anykey');
        $response->assertStatus(200);
        $response->assertJson(["data" => [], "success" => false, "message" => "key not found"]);
    }

    public function testGetKey()
    {
        $this->refreshApplication();
        $this->refreshInMemoryDatabase();
        $faker = Factory::create();
        $faker->seed(rand(1, 100));
        $data     = [];
        $baseDate = Carbon::now()->subHours(15);

        for ($i = 0;$i < 10;$i++) {
            $key    = $faker->name;
            $value  = $faker->text;
            $data[] = [
                'created_at' => Carbon::parse($baseDate)->addHours($i),
                'updated_at' => Carbon::parse($baseDate)->addHours($i),
                'timestamp'  => Carbon::parse($baseDate)->addHours($i)->getTimestamp(),
                'key'        => $key,
                'value'      => $value
            ];
        }
        Secret::insert($data);

        //test latest data
        foreach ($data as $d) {
            $response = $this->getJson('/api/object/' . $d['key']);
            $response->assertStatus(200);
            $response->assertSee($d['value']);
        }

        // test with timestamp value
        foreach ($data as $d) {
            $response = $this->getJson('/api/object/' . $d['key'] . '?timestamp=' . $d['timestamp']);
            $response->assertStatus(200);
            $response->assertSee($d['value']);
        }

        // test for correct key but key has no value from the provided timestamp
        foreach ($data as $d) {
            $timestamp = $d['timestamp'] - 100;
            $response  = $this->getJson('/api/object/' . $d['key'] . '?timestamp=' . $timestamp);
            $response->assertStatus(200);
            $response->assertJson(["data" => [], "success" => false, "message" => "key not found"]);
        }
    }

    public function testKeyPreviousValue()
    {
        $this->refreshApplication();
        $this->refreshInMemoryDatabase();
        $this->refreshDatabase();
        $faker = Factory::create();
        $faker->seed(rand(1, 100));
        $key      = $faker->name;
        $data     = [];
        $baseDate = Carbon::now()->subHours(15);

        for ($i = 1;$i <= 10;$i++) {
            $value  = $faker->text;
            $data[] = [
                'created_at' => Carbon::parse($baseDate)->addHours($i),
                'updated_at' => Carbon::parse($baseDate)->addHours($i),
                'timestamp'  => Carbon::parse($baseDate)->addHours($i)->getTimestamp(),
                'key'        => $key,
                'value'      => $value
            ];
        }
        Secret::insert($data);

        /**
         *
         * Test for the keys previous value
         *
         */
        $oldValue = null;
        foreach ($data as $d) {
            $timestamp = $d['timestamp'] - 1;
            $response  = $this->getJson('/api/object/' . $d['key'] . '?timestamp=' . $timestamp);
            $response->assertStatus(200);
            if (!$oldValue) {
                $response->assertJson(["data" => [], "success" => false, "message" => "key not found"]);
            } else {
                $response->assertSee($oldValue);
            }
            $oldValue = $d['value'];
        }
    }
}
